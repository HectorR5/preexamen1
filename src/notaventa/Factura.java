/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notaventa;

/**
 *
 * @author Hector Ramirez
 */
public class Factura extends NotaVenta implements Impuesto{
    
    private String rfc;
    private String nombre;
    private String domicilio;

    public Factura() {
        this.rfc = "";
        this.nombre = "";
        this.domicilio = "";
    }

    public Factura(String rfc, String nombre, String domicilio, int numNota, String fecha, String concepto, ProductoP perecedero, int cantidad, int tipoPago) {
        super(numNota, fecha, concepto, perecedero, cantidad, tipoPago);
        this.rfc = rfc;
        this.nombre = nombre;
        this.domicilio = domicilio;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
    
    
    
    @Override
    public float calcularPago() {
        return (this.cantidad * perecedero.calcularPrecio());
    }

    @Override
    public float calcularImpuesto() {
        return (this.calcularPago() * 0.16f);
    }
    
    
    
}
