/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notaventa;

/**
 *
 * @author Hector Ramirez
 */
public abstract class Producto {
    protected int id;
    protected String nombreP;
    protected int unidad;
    protected float precioUni;

    public Producto() {
        this.id = 0;
        this.nombreP = "";
        this.unidad = 0;
        this.precioUni = 0.0f;
    }

    public Producto(int id, String nombreP, int unidad, float precioUni) {
        this.id = id;
        this.nombreP = nombreP;
        this.unidad = unidad;
        this.precioUni = precioUni;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreP() {
        return nombreP;
    }

    public void setNombreP(String nombreP) {
        this.nombreP = nombreP;
    }

    public int getUnidad() {
        return unidad;
    }

    public void setUnidad(int unidad) {
        this.unidad = unidad;
    }

    public float getPrecioUni() {
        return precioUni;
    }

    public void setPrecioUni(float precioUni) {
        this.precioUni = precioUni;
    }
    
    public abstract float calcularPrecio();
    
    
    
}
