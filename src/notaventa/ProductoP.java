/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notaventa;

/**
 *
 * @author Hector Ramirez
 */
public class ProductoP extends Producto{
    
    private String fechaC;
    private float temp;

    public ProductoP() {
        this.fechaC = "";
        this.temp = 0.0f;
    }

    public ProductoP(String fechaC, float temp, int id, String nombreP, int unidad, float precioUni) {
        super(id, nombreP, unidad, precioUni);
        this.fechaC = fechaC;
        this.temp = temp;
    }

    public String getFechaC() {
        return fechaC;
    }

    public void setFechaC(String fachaC) {
        this.fechaC = fachaC;
    }

    public float getTemp() {
        return temp;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }
    
    
    
    @Override
    public float calcularPrecio() {
        float pt=0.0f;
        if(this.unidad==0) pt = (this.precioUni * 1.53f);
        if(this.unidad==1) pt = (this.precioUni * 1.55f);
        if(this.unidad==2) pt = (this.precioUni * 1.54f);
        return pt;
    }
    
}
