/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notaventa;

/**
 *
 * @author Hector Ramirez
 */
public abstract class NotaVenta {

    /**
     * @param args the command line arguments
     */
    protected int numNota;
    protected String fecha;
    protected String concepto;
    Producto perecedero;
    protected int cantidad;
    protected int tipoPago;

    public NotaVenta() {
        this.numNota = 0;
        this.fecha = "";
        this.concepto = "";
        this.perecedero = new ProductoP();
        this.cantidad = 0;
        this.tipoPago = 0;
    }

    public NotaVenta(int numNota, String fecha, String concepto, Producto perecedero, int cantidad, int tipoPago) {
        this.numNota = numNota;
        this.fecha = fecha;
        this.concepto = concepto;
        this.perecedero = perecedero;
        this.cantidad = cantidad;
        this.tipoPago = tipoPago;
    }

    public int getNumNota() {
        return numNota;
    }

    public void setNumNota(int numNota) {
        this.numNota = numNota;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Producto getPerecedero() {
        return perecedero;
    }

    public void setPerecedero(Producto perecedero) {
        this.perecedero = perecedero;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(int tipoPago) {
        this.tipoPago = tipoPago;
    }
    
    public abstract float calcularPago();
    
}
