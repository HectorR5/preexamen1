/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notaventa;

/**
 *
 * @author Hector Ramirez
 */
public class ProductoNP extends Producto {
    
    private String lote;

    public ProductoNP() {
        this.lote = "";
    }

    public ProductoNP(String lote, int id, String nombreP, int unidad, float precioUni) {
        super(id, nombreP, unidad, precioUni);
        this.lote = lote;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    @Override
    public float calcularPrecio() {
        return this.precioUni * 1.5f;
    }
    
}